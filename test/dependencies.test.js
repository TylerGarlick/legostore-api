const db = {
  query() {
    return []
  },

  filtered() {
    return this.query().filter(p => p.isActive)
  }
}

describe(`An example of Mocking`, () => [

  describe(`Query`, () => {

    it(`empty by default`, () => expect(db.query()).to.be.empty())

    it(`works differently with classes`, () => {
      db.query = () => []
      expect(db.query()).to.be.empty()
    })

    it(`filters by isActive`, () => {
      db.query = () => [{
        name: 'Something',
        isActive: true
      },
        {
          name: 'Another Thing',
          isActive: false
        }]

      expect(db.filtered().length).to.be.equal(1)
    })

  })

])
