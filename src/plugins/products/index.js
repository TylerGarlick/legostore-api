import pkg from './package'
import Routes from './routes'

export default {
  pkg,
  dependencies: ['persistence'],
  
  async register(server, options = {}) {

    server.dependency(this.dependencies, server => {
      const { db } = server.plugins.persistence
      const collection = db.get('shoes')

      server.expose({
        collection
      })

      server.route(Routes)
    })
  }
}
