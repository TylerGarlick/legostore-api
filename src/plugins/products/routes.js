export default [
  {
    method: 'GET',
    path: '/v1/products',
    options: {},
    async handler(request, h) {
      const { collection } = request.server.plugins.products
      return await collection.find({})
    }
  }
]
